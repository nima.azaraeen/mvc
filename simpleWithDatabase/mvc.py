from StudentModel import Student
from StudentView import CLIView
from StudentController import StudentController

if __name__ == '__main__':
    model = Student()
    view = CLIView()
    controller = StudentController(model, view)

    while True:
        choice = view.main_menu()
        if choice == '1':
            student_id = view.get_student_id()
            controller.get_student_by_id(student_id)
        elif choice == '2':
            controller.list_students()
        elif choice == '3':
            name, rollNo = view.get_student_details()
            controller.add_student(name, rollNo)
        elif choice == '4':
            break
        else:
            print("Invalid choice!")
