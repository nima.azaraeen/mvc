import pymysql

class Student:
    def __init__(self, id=None, name=None, rollNo=None):
        self.id = id
        self.name = name
        self.rollNo = rollNo
        self.connection = pymysql.connect(host='localhost',
                                          user='root',
                                          password='randomPass@min',
                                          database='students')
        self.cursor = self.connection.cursor()

    def save(self):
        if not self.id:
            self.cursor.execute("INSERT INTO students (name, rollNo) VALUES (%s, %s)", (self.name, self.rollNo))
            self.id = self.cursor.lastrowid
            self.connection.commit()
        else:
            self.cursor.execute("UPDATE students SET name=%s, rollNo=%s WHERE id=%s", (self.name, self.rollNo, self.id))
            self.connection.commit()

    def fetch(self, student_id):
        self.cursor.execute("SELECT id, name, rollNo FROM students WHERE id=%s", (student_id,))
        result = self.cursor.fetchone()
        if result:
            self.id, self.name, self.rollNo = result
        else:
            self.id, self.name, self.rollNo = None, None, None

    def fetch_all(self):
        self.cursor.execute("SELECT id, name, rollNo FROM students")
        return self.cursor.fetchall()

    def __del__(self):
        self.connection.close()
