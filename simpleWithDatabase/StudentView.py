class CLIView:
    def main_menu(self):
        print("1. Get student by ID")
        print("2. List students")
        print("3. Add new students")
        print("4. Exit")
        return input("Choose option (1/2/3/4): ")

    def get_student_id(self):
        return input("Enter student ID: ")

    def get_student_details(self):
        name = input("Enter student name: ")
        rollNo = input("Enter student roll number: ")
        return name, rollNo

    def display_student(self, student):
        print("\nStudent Details:")
        print(f"ID: {student[0]}")
        print(f"Name: {student[1]}")
        print(f"Roll Number: {student[2]}\n")

    def display_all_students(self, students):
        print("\nAll Students:")
        for student in students:
            self.display_student(student)