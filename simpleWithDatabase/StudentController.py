from StudentModel import Student

class StudentController:
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def add_student(self, name, rollNo):
        self.model = Student()  # Create a new student object
        self.model.name = name
        self.model.rollNo = rollNo
        self.model.save()

    def get_student_by_id(self, student_id):
        self.model.fetch(student_id)
        if self.model.id:
            self.view.display_student((self.model.id, self.model.name, self.model.rollNo))
        else:
            print("No student found with that ID!")

    def list_students(self):
        students = self.model.fetch_all()
        self.view.display_all_students(students)
