from Student import Student
from StudentView import StudentView
from StudentController import StudentController

def retriveStudentFromDatabase():
    student = Student("Lokesh Sharma", "15UCS157")
    return student

if __name__ == '__main__':
    model  = retriveStudentFromDatabase()
    view = StudentView()
    controller = StudentController(model, view)
    controller.updateView()
    controller.setStudentName("Vikram Sharma")
    controller.updateView()