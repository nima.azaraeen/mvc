class StudentController:
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def setStudentName(self, name):
        self.model.setName(name)

    def getStudentName(self):
        return self.model.getName()

    def setStudentRollNo(self, rollNo):
        self.model.setRollNo(rollNo)

    def getStudentRollNo(self):
        return self.model.getRollNo()

    def updateView(self):
        self.view.printStudentDetails(self.model.getName(), self.model.getRollNo())