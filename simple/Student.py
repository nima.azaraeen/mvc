class Student:
    def __init__(self, name, rollNo):
        self.name = name
        self.rollNo = rollNo

    def getName(self):
        return self.name

    def getRollNo(self):
        return self.rollNo

    def setName(self, name):
        self.name = name

    def setRollNo(self, rollNo):
        self.rollNo = rollNo